var active = {
    //step animation on slide 2
    stepCounter: 0,
    stepCowsAnimation: function () {
        active.stepCounter = 0;
        var clickDisabled = false;
        $('.p2').on('click', function () {
            $('.unhide').removeClass('reset_animate');
            if (clickDisabled) return;
            active.stepCounter++;
            if (active.stepCounter <= 3) {
                $('.p2').find('.slide-content').addClass('animate_step_' + active.stepCounter);
                if (active.stepCounter === 1) {
                    $('.first_text,.second_text').removeClass('show');
                    $('.percent').stop().prop('count', 0).animate({
                        count: 10
                    }, {
                        duration: 1500,
                        easing: 'linear',
                        step: function (num) {
                            $('.percent').text(Math.ceil(num) + '%');
                            if (num === 10) {
                                $('.first_text').addClass('show');
                            }
                        }
                    });
                }
                else if (active.stepCounter === 2) {
                    $('.percent').stop().prop('count', 10).animate({
                        count: 50,
                    }, {
                        duration: 1500,
                        easing: 'linear',
                        step: function (num) {
                            $('.percent').text(Math.ceil(num) + '%');
                            if (num === 50) {
                                $('.first_text').removeClass('show');
                                $('.second_text').addClass('show');
                            }
                        }
                    });
                }
            }
            clickDisabled = true;
            setTimeout(function () {
                clickDisabled = false;
            }, 2000);
        });
    }
};

$(document).ready(function () {
    active.stepCowsAnimation();
});
